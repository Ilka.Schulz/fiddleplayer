# FiddlePlayer

This is an old piece of software I found on my disk. Code quality is pretty terrible and I will attend to it when I find time. Treat with caution...

FiddlePlayer aims to be a Linux GUI software that allows the user to edit the meta data of their music files. It may also function as a music player in the future.

### Build

1. Install `id3v2`: `sudo apt install id3v2`
2. Install QT packages: `sudo apt install qtmultimedia5-dev`
3. Build with [QT's build chain](https://stackoverflow.com/questions/6127357/building-qt-creator-projects-from-command-line)

### License

FiddlePlayer is licensed under the GNU General Public License version 3. Also see the [COPYING](COPYING) file.
