/// @file

#include "Preferences.hpp"

Preferences *Preferences::instance = nullptr;

Preferences::Preferences()
        // default setting
        :  geniusUpdateArtist(true),
           geniusUpdateAlbum(true),
           geniusUpdateTitle(true),
           geniusUpdateYear(true),
           geniusUpdateGenre(true),
           geniusUpdateFilename(false),
           geniusSearchWithArtist(true),
           geniusSearchWithAlbum(false),
           geniusSearchWithTitle(true),
           geniusSearchWithYear(false),
           geniusSearchWithGenre(false),
           geniusSearchWithFilename(false),
           geniusAlwaysUseFirstHit(true)  {
    /// @todo read from file
}
Preferences *Preferences::getInstance()  {
    if(instance == nullptr)
        instance = new Preferences();
    return instance;
}
void Preferences::informObserver(PreferencesObserver *observer)  {
    observer->preferenceChanged_geniusUpdateArtist(geniusUpdateArtist);
    observer->preferenceChanged_geniusUpdateAlbum(geniusUpdateAlbum);
    observer->preferenceChanged_geniusUpdateTitle(geniusUpdateTitle);
    observer->preferenceChanged_geniusUpdateYear(geniusUpdateYear);
    observer->preferenceChanged_geniusUpdateGenre(geniusUpdateGenre);
    observer->preferenceChanged_geniusUpdateFilename(geniusUpdateFilename);

    observer->preferenceChanged_geniusSearchWithArtist(geniusSearchWithArtist);
    observer->preferenceChanged_geniusSearchWithAlbum(geniusSearchWithAlbum);
    observer->preferenceChanged_geniusSearchWithTitle(geniusSearchWithTitle);
    observer->preferenceChanged_geniusSearchWithYear(geniusSearchWithYear);
    observer->preferenceChanged_geniusSearchWithGenre(geniusSearchWithGenre);
    observer->preferenceChanged_geniusSearchWithFilename(geniusSearchWithFilename);

    observer->preferenceChanged_geniusAlwaysUseFirstHit(geniusAlwaysUseFirstHit);
}

bool Preferences::getSetting_geniusUpdateArtist()    { return geniusUpdateArtist; }
bool Preferences::getSetting_geniusUpdateAlbum()     { return geniusUpdateAlbum; }
bool Preferences::getSetting_geniusUpdateTitle()     { return geniusUpdateTitle; }
bool Preferences::getSetting_geniusUpdateYear()      { return geniusUpdateYear; }
bool Preferences::getSetting_geniusUpdateGenre()     { return geniusUpdateGenre; }
bool Preferences::getSetting_geniusUpdateFilename()  { return geniusUpdateFilename; }
bool Preferences::getSetting_geniusSearchWithArtist()    { return geniusSearchWithArtist; }
bool Preferences::getSetting_geniusSearchWithAlbum()     { return geniusSearchWithAlbum; }
bool Preferences::getSetting_geniusSearchWithTitle()     { return geniusSearchWithTitle; }
bool Preferences::getSetting_geniusSearchWithYear()      { return geniusSearchWithYear; }
bool Preferences::getSetting_geniusSearchWithGenre()     { return geniusSearchWithGenre; }
bool Preferences::getSetting_geniusSearchWithFilename()  { return geniusSearchWithFilename; }
bool Preferences::getSetting_geniusAlwaysUseFirstHit()  { return geniusAlwaysUseFirstHit; }

void Preferences::setSetting_geniusUpdateArtist(bool value)    {
    geniusUpdateArtist = value;
    for(PreferencesObserver *observer : observers)
        observer->preferenceChanged_geniusUpdateArtist(value);
}
void Preferences::setSetting_geniusUpdateAlbum(bool value)     {
    geniusUpdateAlbum = value;
    for(PreferencesObserver *observer : observers)
        observer->preferenceChanged_geniusUpdateAlbum(value);
}
void Preferences::setSetting_geniusUpdateTitle(bool value)     {
    geniusUpdateTitle = value;
    for(PreferencesObserver *observer : observers)
        observer->preferenceChanged_geniusUpdateTitle(value);
}
void Preferences::setSetting_geniusUpdateYear(bool value)      {
    geniusUpdateYear = value;
    for(PreferencesObserver *observer : observers)
        observer->preferenceChanged_geniusUpdateYear(value);
}
void Preferences::setSetting_geniusUpdateGenre(bool value)     {
    geniusUpdateGenre = value;
    for(PreferencesObserver *observer : observers)
        observer->preferenceChanged_geniusUpdateGenre(value);
}
void Preferences::setSetting_geniusUpdateFilename(bool value)  {
    geniusUpdateFilename = value;
    for(PreferencesObserver *observer : observers)
        observer->preferenceChanged_geniusUpdateFilename(value);
}
void Preferences::setSetting_geniusSearchWithArtist(bool value)    {
    geniusSearchWithArtist = value;
    for(PreferencesObserver *observer : observers)
        observer->preferenceChanged_geniusSearchWithArtist(value);
}
void Preferences::setSetting_geniusSearchWithAlbum(bool value)     {
    geniusSearchWithAlbum = value;
    for(PreferencesObserver *observer : observers)
        observer->preferenceChanged_geniusSearchWithAlbum(value);
}
void Preferences::setSetting_geniusSearchWithTitle(bool value)     {
    geniusSearchWithTitle = value;
    for(PreferencesObserver *observer : observers)
        observer->preferenceChanged_geniusSearchWithTitle(value);
}
void Preferences::setSetting_geniusSearchWithYear(bool value)      {
    geniusSearchWithYear = value;
    for(PreferencesObserver *observer : observers)
        observer->preferenceChanged_geniusSearchWithYear(value);
}
void Preferences::setSetting_geniusSearchWithGenre(bool value)     {
    geniusSearchWithGenre = value;
    for(PreferencesObserver *observer : observers)
        observer->preferenceChanged_geniusSearchWithGenre(value);
}
void Preferences::setSetting_geniusSearchWithFilename(bool value)  {
    geniusSearchWithFilename = value;
    for(PreferencesObserver *observer : observers)
        observer->preferenceChanged_geniusSearchWithFilename(value);
}
void Preferences::setSetting_geniusAlwaysUseFirstHit(bool value)  {
    geniusAlwaysUseFirstHit = value;
    for(PreferencesObserver *observer : observers)
        observer->preferenceChanged_geniusAlwaysUseFirstHit(value);
}
