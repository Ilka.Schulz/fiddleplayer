#include "Application.hpp"
#include "src/util/Exception.hpp"

Application::Application(int &argc, char **argv)
    :  QApplication(argc,argv)  {
}

bool Application::notify(QObject *sender, QEvent *event)  {
    try  {
        return QApplication::notify(sender, event);
    }
    catch(TException &e)  {
        e.show();
        return false;
    }
}
