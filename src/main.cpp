#include "src/Application.hpp"
#include "src/ui/Mainwindow.hpp"
#include "src/util/Exception.hpp"

/// @file

int main(int argc, char *argv[])
{
    /// @todo check if id3v2, VLC, Audacity and gio are installed
    Application application(argc, argv);
    TMainWindow mainWindow;
    if(argc>=2)  {
        mainWindow.selectPath(argv[1]);
    }
    mainWindow.show();
    while(true)
    {
        try { return application.exec(); }
        catch(TException &e) { e.show(); }
    }
    return 1;
}
