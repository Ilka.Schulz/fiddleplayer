/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <vector>
#include <algorithm>

/** @brief base class for observable classes
 *  @details Every observable class @c Class must have a partner class @c ClassObserver that has virtual event handler
 *  methods. Observer classes can than inherit from @c ClassObserver. The observable class must inherit from
 *  @c Observable<ClassObserver> and call the virtual handler methods for all observer objects. \n
 *  - Design pattern @c Oberserver -> class "Subject"
 */
template<typename ObserverType>
class Observable  {
    protected:
        /// list of oberserver objects
        std::vector<ObserverType *> observers;
    public:
        /// adds an observer to @c Observable::observers  @param observer  the observer to add
        void addObserver(ObserverType *observer);

        /** @brief removes an observer from @c Observable::observers
         *  @details This function does not do anything if the specified observer is not in the @c Observable::observers
         *  list.
         *  @param observer  the observer to remove
         */
        void removeObserver(ObserverType *observer);
};





template<typename ObserverType>
void Observable<ObserverType>::addObserver(ObserverType *observer)  {
    observers.push_back(observer);
}

template<typename ObserverType>
void Observable<ObserverType>::removeObserver(ObserverType *observer)  {
    auto itr = std::find(observers.begin(), observers.end(), observer);
    if(itr != observers.end())
        observers.erase(itr);
}
