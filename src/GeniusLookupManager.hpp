/// @file

#include <QNetworkAccessManager>
#include <QTimer>
#include <vector>
#include <map>

class TFile;

class GeniusLookupManager  :  public QNetworkAccessManager  {
    private:
        QTimer timer;
        std::vector<QUrl> network_request_todo;
        std::map<QString,TFile*> network_requests;

    public:
        GeniusLookupManager(QObject *parent);

    public:
        void lookUp(TFile *file);
        int getActiveRequestsCount()  const;
        int getQueuedRequestsCount()  const;

    private:
        std::map<QString,QString> readTags(QString raw);
        static QString findTag(std::map<QString,QString> data, QString key);
        void on_networkFinished(QNetworkReply* reply);
        void on_timeout();
};
