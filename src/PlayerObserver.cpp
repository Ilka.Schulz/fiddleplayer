/// @file

#include "PlayerObserver.hpp"

// do nothing by default
void PlayerObserver::playerEvent_fileSelected(Player *player)  { }
void PlayerObserver::playerEvent_started(Player *player)  { }
void PlayerObserver::playerEvent_paused(Player *player)  { }
void PlayerObserver::playerEvent_timeStampChanged(Player *player)  { }
void PlayerObserver::playerEvent_durationChanged(Player *player)  { }
void PlayerObserver::playerEvent_stopped(Player *player)  { }
void PlayerObserver::playerEvent_loopModeChanged(Player *player)  { }
void PlayerObserver::playerEvent_shuffleModeChanged(Player *player)  { }
void PlayerObserver::playerEvent_volumeChanged(Player *player)  { }
