/// @file

#pragma once

#include <QBoxLayout>
#include <QLabel>
#include <QSlider>
#include <QToolButton>

#include "src/Player.hpp"
#include "src/PlayerObserver.hpp"

/// @todo add method setPlayer(Player*)
class PlayerWidget  :  public QWidget, protected PlayerObserver  {
    private:
        Player player;
        QBoxLayout *verticalLayout;
            QBoxLayout *sliderLayout;
                QLabel *timeStampLabel;
                QSlider *horizontalSlider;
                    bool currentlyMovingHorizontalSlider;  ///< whether the horizontal slider is currently moved programatically so that the event handler shall ignore the event
                QLabel *durationLabel;
            QBoxLayout *buttonsLayout;
                QToolButton *playButton;
                QToolButton *previousButton;
                QToolButton *stopButton;
                QToolButton *nextButton;
                QToolButton *loopButton;
                QToolButton *shuffleButton;
    public:
        PlayerWidget(QWidget *parent=nullptr);
        ~PlayerWidget();
    public:
        void selectFile(TFile *file);
        void start();
    private:
        QString timeToString(qint64 timeStamp);
    private slots:
        void playButtonClickedEvent(bool checked);
        void previousButtonClickedEvent(bool checked);
        void stopButtonClickedEvent(bool checked);
        void nextButtonClickedEvent(bool checked);
        void loopButtonClickedEvent(bool checked);
        void shuffleButtonClickedEvent(bool checked);
        void horizontalSliderValueChangedEvent(int value);

    private:
        // player observation events
        virtual void playerEvent_fileSelected(Player*)  override;
        virtual void playerEvent_started(Player*)  override;
        virtual void playerEvent_paused(Player *)  override;
        virtual void playerEvent_timeStampChanged(Player *)  override;
        virtual void playerEvent_durationChanged(Player *)  override;
        virtual void playerEvent_stopped(Player *)  override;
        virtual void playerEvent_loopModeChanged(Player *)  override;
        virtual void playerEvent_shuffleModeChanged(Player *)  override;
        virtual void playerEvent_volumeChanged(Player *)  override;

};
