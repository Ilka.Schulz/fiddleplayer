#include "Windowdecideurl.hpp"
#include "qlistview.h"

#include <QGridLayout>
#include <QLabel>
#include <QListWidget>
#include <QListWidgetItem>
#include <QPushButton>

int TWindowDecideUrl::current_instances=0;

TWindowDecideUrl::TWindowDecideUrl(QWidget *parent) :
        QDialog(parent)  {
    //ui->setupUi(this);
    rootGrid = new QGridLayout(this);
    setLayout(rootGrid);
        // question label
        questionLabel = new QLabel(nullptr);
        questionLabel->setText(tr("Which URL fits the song?"));
        rootGrid->addWidget(questionLabel, 0,0, 1,2);
        // title question label
        titleQuestionLabel = new QLabel(nullptr);
        titleQuestionLabel->setText(tr("Title:"));
        rootGrid->addWidget(titleQuestionLabel, 1,0);
        // title label
        titleLabel = new QLabel(nullptr);
        titleLabel->setText("dummy");
        rootGrid->addWidget(titleLabel, 1,1);
        // artist question label
        artistQuestionLabel = new QLabel(nullptr);
        artistQuestionLabel->setText(tr("Artist:"));
        rootGrid->addWidget(artistQuestionLabel, 2,0);
        // artist label
        artistLabel = new QLabel(nullptr);
        artistLabel->setText("dummy");
        rootGrid->addWidget(artistLabel, 2,1);
        // hint label
        hintLabel = new QLabel(nullptr);
        hintLabel->setText(tr("(double click the according entry)"));
        rootGrid->addWidget(hintLabel, 2,0, 1,2);
        // URL list widget
        urlListWidget = new QListWidget(nullptr);
        rootGrid->addWidget(urlListWidget, 3,0, 1,2);
        // cancel button
        cancelButton = new QPushButton(nullptr);
        cancelButton->setText(tr("Cancel"));
        rootGrid->addWidget(cancelButton, 4,0, 1,2);

    current_instances++;
}

TWindowDecideUrl::~TWindowDecideUrl()  {
    current_instances--;
}

QString TWindowDecideUrl::execute(QString artist, QString title, QStringList urls)  {
    TWindowDecideUrl *w = new TWindowDecideUrl();
    w->artistLabel->setText(artist);
    w->titleLabel->setText(title);
    for(QString s : urls)
        w->urlListWidget->addItem(s);
    int retcode = w->exec();
    delete w;
    if(retcode==QDialog::Rejected)
        return "";
    else
        return w->selected_url;
}

int TWindowDecideUrl::getCurrentInstances()  {
    return current_instances;
}

void TWindowDecideUrl::closeEvent(QCloseEvent *event)  {
    //deleteLater();
}

void TWindowDecideUrl::on_pushButton_Cancel_clicked() {
    reject();
}

void TWindowDecideUrl::on_listWidget_Urls_itemDoubleClicked(QListWidgetItem *item) {
    selected_url = item->text();
    accept();
}
