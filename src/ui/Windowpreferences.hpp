#pragma once

#include <QDialog>

#include "src/PreferencesObserver.hpp"

class QTabWidget;
class QWidget;
class QGridLayout;
class QCheckBox;
class QFrame;
class QPushButton;

class TWindowPreferences : public QDialog, protected PreferencesObserver  {
    private:
        QGridLayout *rootLayout;
        QTabWidget *tabWidget;
            QWidget *generalTab;
            QWidget *geniusTab;
                QGridLayout *geniusTabGrid;
                    QCheckBox *geniusTabUpdateArtistCheckbox;
                    QCheckBox *geniusTabUpdateAlbumCheckbox;
                    QCheckBox *geniusTabUpdateTitleCheckbox;
                    QCheckBox *geniusTabUpdateYearCheckbox;
                    QCheckBox *geniusTabUpdateGenreCheckbox;
                    QCheckBox *geniusTabUpdateFilenameCheckbox;
                    QCheckBox *geniusTabSearchWithArtistCheckbox;
                    QCheckBox *geniusTabSearchWithAlbumCheckbox;
                    QCheckBox *geniusTabSearchWithTitleCheckbox;
                    QCheckBox *geniusTabSearchWithYearCheckbox;
                    QCheckBox *geniusTabSearchWithGenreCheckbox;
                    QCheckBox *geniusTabSearchWithFilenameCheckbox;
                    QFrame *geniusTabHorizontalLine;
                    QCheckBox *geniusTabAlwaysUseFirstHitCheckbox;
        QPushButton *okButton;

    private:
        explicit TWindowPreferences(QWidget *parent = nullptr);
        ~TWindowPreferences();
    public:
        static TWindowPreferences* instance();

    private slots:
        // ui handlers
        void geniusTabUpdateArtistCheckboxStateChangedEvent(int state);
        void geniusTabUpdateAlbumCheckboxStateChangedEvent(int state);
        void geniusTabUpdateTitleCheckboxStateChangedEvent(int state);
        void geniusTabUpdateYearCheckboxStateChangedEvent(int state);
        void geniusTabUpdateGenreCheckboxStateChangedEvent(int state);
        void geniusTabUpdateFilenameCheckboxStateChangedEvent(int state);

        void geniusTabSearchWithArtistCheckboxStateChangedEvent(int state);
        void geniusTabSearchWithAlbumCheckboxStateChangedEvent(int state);
        void geniusTabSearchWithTitleCheckboxStateChangedEvent(int state);
        void geniusTabSearchWithYearCheckboxStateChangedEvent(int state);
        void geniusTabSearchWithGenreCheckboxStateChangedEvent(int state);
        void geniusTabSearchWithFilenameCheckboxStateChangedEvent(int state);

        void geniusTabAlwaysUseFirstHitCheckboxStateChangedEvent(int state);

    protected:
        // observer handlers
        virtual void preferenceChanged_geniusUpdateArtist(bool value)  override;
        virtual void preferenceChanged_geniusUpdateAlbum(bool value)  override;
        virtual void preferenceChanged_geniusUpdateTitle(bool value)  override;
        virtual void preferenceChanged_geniusUpdateYear(bool value)  override;
        virtual void preferenceChanged_geniusUpdateGenre(bool value)  override;
        virtual void preferenceChanged_geniusUpdateFilename(bool value)  override;

        virtual void preferenceChanged_geniusSearchWithArtist(bool value)  override;
        virtual void preferenceChanged_geniusSearchWithAlbum(bool value)  override;
        virtual void preferenceChanged_geniusSearchWithTitle(bool value)  override;
        virtual void preferenceChanged_geniusSearchWithYear(bool value)  override;
        virtual void preferenceChanged_geniusSearchWithGenre(bool value)  override;
        virtual void preferenceChanged_geniusSearchWithFilename(bool value)  override;

        virtual void preferenceChanged_geniusAlwaysUseFirstHit(bool value)  override;

    private slots:

};
