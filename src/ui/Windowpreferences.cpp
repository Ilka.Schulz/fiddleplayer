#include "Windowpreferences.hpp"
#include <QTabWidget>
#include <QWidget>
#include <QGridLayout>
#include <QCheckBox>
#include <QFrame>
#include <QPushButton>

#include "src/Preferences.hpp"

TWindowPreferences::TWindowPreferences(QWidget *parent) :
        QDialog(parent)  {
    /// @todo hard-coded checkbox states
    /// @todo event handlers
    rootLayout = new QGridLayout(this);
    setLayout(rootLayout);
    tabWidget = new QTabWidget(this);
    rootLayout->addWidget(tabWidget);
        // general tab
        QWidget *tabGeneral = new QWidget();
        tabWidget->addTab(tabGeneral, tr("General"));
        // Genius tab
        QWidget *tabGenius = new QWidget();
        tabWidget->addTab(tabGenius, tr("Genius"));
            // grid layout
            geniusTabGrid = new QGridLayout(tabGenius);
                // update artist checkbox
                geniusTabUpdateArtistCheckbox = new QCheckBox(tabGenius);
                geniusTabUpdateArtistCheckbox->setText(tr("update artist"));
                geniusTabGrid->addWidget(geniusTabUpdateArtistCheckbox);
                connect(geniusTabUpdateArtistCheckbox, &QCheckBox::stateChanged,
                        this, &TWindowPreferences::geniusTabUpdateArtistCheckboxStateChangedEvent);
                // update album checkbox
                geniusTabUpdateAlbumCheckbox = new QCheckBox(tabGenius);
                geniusTabUpdateAlbumCheckbox->setText(tr("update album"));
                geniusTabGrid->addWidget(geniusTabUpdateAlbumCheckbox);
                connect(geniusTabUpdateAlbumCheckbox, &QCheckBox::stateChanged,
                        this, &TWindowPreferences::geniusTabUpdateAlbumCheckboxStateChangedEvent);
                // update title checkbox
                geniusTabUpdateTitleCheckbox = new QCheckBox(tabGenius);
                geniusTabUpdateTitleCheckbox->setText(tr("update title"));
                geniusTabGrid->addWidget(geniusTabUpdateTitleCheckbox);
                connect(geniusTabUpdateTitleCheckbox, &QCheckBox::stateChanged,
                        this, &TWindowPreferences::geniusTabUpdateTitleCheckboxStateChangedEvent);
                // update year checkbox
                geniusTabUpdateYearCheckbox = new QCheckBox(tabGenius);
                geniusTabUpdateYearCheckbox->setText(tr("update year"));
                geniusTabGrid->addWidget(geniusTabUpdateYearCheckbox);
                connect(geniusTabUpdateYearCheckbox, &QCheckBox::stateChanged,
                        this, &TWindowPreferences::geniusTabUpdateYearCheckboxStateChangedEvent);
                // update genre checkbox
                geniusTabUpdateGenreCheckbox = new QCheckBox(tabGenius);
                geniusTabUpdateGenreCheckbox->setText(tr("update genre"));
                geniusTabGrid->addWidget(geniusTabUpdateGenreCheckbox);
                connect(geniusTabUpdateGenreCheckbox, &QCheckBox::stateChanged,
                        this, &TWindowPreferences::geniusTabUpdateGenreCheckboxStateChangedEvent);
                // update file name checkbox
                geniusTabUpdateFilenameCheckbox = new QCheckBox(tabGenius);
                geniusTabUpdateFilenameCheckbox->setText(tr("update file name"));
                geniusTabGrid->addWidget(geniusTabUpdateFilenameCheckbox);
                connect(geniusTabUpdateGenreCheckbox, &QCheckBox::stateChanged,
                        this, &TWindowPreferences::geniusTabUpdateGenreCheckboxStateChangedEvent);

                // search with artist checkbox
                geniusTabSearchWithArtistCheckbox = new QCheckBox(tabGenius);
                geniusTabSearchWithArtistCheckbox->setText(tr("search with artist"));
                geniusTabGrid->addWidget(geniusTabSearchWithArtistCheckbox, 0,1);
                connect(geniusTabSearchWithArtistCheckbox, &QCheckBox::stateChanged,
                        this, &TWindowPreferences::geniusTabSearchWithArtistCheckboxStateChangedEvent);
                // search with album checkbox
                geniusTabSearchWithAlbumCheckbox = new QCheckBox(tabGenius);
                geniusTabSearchWithAlbumCheckbox->setText(tr("search with album"));
                geniusTabGrid->addWidget(geniusTabSearchWithAlbumCheckbox, 1,1);
                connect(geniusTabSearchWithAlbumCheckbox, &QCheckBox::stateChanged,
                        this, &TWindowPreferences::geniusTabSearchWithAlbumCheckboxStateChangedEvent);
                // search with title checkbox
                geniusTabSearchWithTitleCheckbox = new QCheckBox(tabGenius);
                geniusTabSearchWithTitleCheckbox->setText(tr("search with title"));
                geniusTabGrid->addWidget(geniusTabSearchWithTitleCheckbox, 2,1);
                connect(geniusTabSearchWithTitleCheckbox, &QCheckBox::stateChanged,
                        this, &TWindowPreferences::geniusTabSearchWithTitleCheckboxStateChangedEvent);
                // search with year checkbox
                geniusTabSearchWithYearCheckbox = new QCheckBox(tabGenius);
                geniusTabSearchWithYearCheckbox->setText(tr("search with year"));
                geniusTabGrid->addWidget(geniusTabSearchWithYearCheckbox, 3,1);
                connect(geniusTabSearchWithYearCheckbox, &QCheckBox::stateChanged,
                        this, &TWindowPreferences::geniusTabSearchWithYearCheckboxStateChangedEvent);
                // search with genre checkbox
                geniusTabSearchWithGenreCheckbox = new QCheckBox(tabGenius);
                geniusTabSearchWithGenreCheckbox->setText(tr("search with genre"));
                geniusTabGrid->addWidget(geniusTabSearchWithGenreCheckbox, 4,1);
                connect(geniusTabSearchWithGenreCheckbox, &QCheckBox::stateChanged,
                        this, &TWindowPreferences::geniusTabSearchWithGenreCheckboxStateChangedEvent);
                // search with file name checkbox
                geniusTabSearchWithFilenameCheckbox = new QCheckBox(tabGenius);
                geniusTabSearchWithFilenameCheckbox->setText(tr("search with file name"));
                geniusTabGrid->addWidget(geniusTabSearchWithFilenameCheckbox, 5,1);
                connect(geniusTabSearchWithFilenameCheckbox, &QCheckBox::stateChanged,
                        this, &TWindowPreferences::geniusTabSearchWithFilenameCheckboxStateChangedEvent);

                // horizontal line
                geniusTabHorizontalLine = new QFrame(tabGenius);
                geniusTabHorizontalLine->setFrameShape(QFrame::HLine);
                geniusTabHorizontalLine->setFrameShadow(QFrame::Sunken);
                geniusTabGrid->addWidget(geniusTabHorizontalLine, 6,0, 1,2);

                // always use first hit checkbox
                geniusTabAlwaysUseFirstHitCheckbox = new QCheckBox(tabGenius);
                geniusTabAlwaysUseFirstHitCheckbox->setText(tr("always use first search result"));
                geniusTabGrid->addWidget(geniusTabAlwaysUseFirstHitCheckbox, 7,0, 1,2);
                connect(geniusTabAlwaysUseFirstHitCheckbox, &QCheckBox::stateChanged,
                        this, &TWindowPreferences::geniusTabAlwaysUseFirstHitCheckboxStateChangedEvent);

    tabWidget->setCurrentWidget(tabGenius);
    Preferences::getInstance()->addObserver(this);
    Preferences::getInstance()->informObserver(this);
}

TWindowPreferences::~TWindowPreferences()  {
    Preferences::getInstance()->removeObserver(this);
}

TWindowPreferences *TWindowPreferences::instance()  {
    static TWindowPreferences w;
    return &w;
}

// ui handlers
void TWindowPreferences::geniusTabUpdateArtistCheckboxStateChangedEvent(int state)    {
    Preferences::getInstance()->setSetting_geniusUpdateArtist(state==Qt::CheckState::Checked);
}
void TWindowPreferences::geniusTabUpdateAlbumCheckboxStateChangedEvent(int state)     {
    Preferences::getInstance()->setSetting_geniusUpdateAlbum(state==Qt::CheckState::Checked);
}
void TWindowPreferences::geniusTabUpdateTitleCheckboxStateChangedEvent(int state)     {
    Preferences::getInstance()->setSetting_geniusUpdateTitle(state==Qt::CheckState::Checked);
}
void TWindowPreferences::geniusTabUpdateYearCheckboxStateChangedEvent(int state)      {
    Preferences::getInstance()->setSetting_geniusUpdateYear(state==Qt::CheckState::Checked);
}
void TWindowPreferences::geniusTabUpdateGenreCheckboxStateChangedEvent(int state)     {
    Preferences::getInstance()->setSetting_geniusUpdateGenre(state==Qt::CheckState::Checked);
}
void TWindowPreferences::geniusTabUpdateFilenameCheckboxStateChangedEvent(int state)  {
    Preferences::getInstance()->setSetting_geniusUpdateFilename(state==Qt::CheckState::Checked);
}
void TWindowPreferences::geniusTabSearchWithArtistCheckboxStateChangedEvent(int state)    {
    Preferences::getInstance()->setSetting_geniusSearchWithArtist(state==Qt::CheckState::Checked);
}
void TWindowPreferences::geniusTabSearchWithAlbumCheckboxStateChangedEvent(int state)     {
    Preferences::getInstance()->setSetting_geniusSearchWithAlbum(state==Qt::CheckState::Checked);
}
void TWindowPreferences::geniusTabSearchWithTitleCheckboxStateChangedEvent(int state)     {
    Preferences::getInstance()->setSetting_geniusSearchWithTitle(state==Qt::CheckState::Checked);
}
void TWindowPreferences::geniusTabSearchWithYearCheckboxStateChangedEvent(int state)      {
    Preferences::getInstance()->setSetting_geniusSearchWithYear(state==Qt::CheckState::Checked);
}
void TWindowPreferences::geniusTabSearchWithGenreCheckboxStateChangedEvent(int state)     {
    Preferences::getInstance()->setSetting_geniusSearchWithGenre(state==Qt::CheckState::Checked);
}
void TWindowPreferences::geniusTabSearchWithFilenameCheckboxStateChangedEvent(int state)  {
    Preferences::getInstance()->setSetting_geniusSearchWithFilename(state==Qt::CheckState::Checked);
}
void TWindowPreferences::geniusTabAlwaysUseFirstHitCheckboxStateChangedEvent(int state)  {
    Preferences::getInstance()->setSetting_geniusAlwaysUseFirstHit(state==Qt::CheckState::Checked);
}

// observer handlers
void TWindowPreferences::preferenceChanged_geniusUpdateArtist(bool value)    {
    geniusTabUpdateArtistCheckbox->setChecked(value);
}
void TWindowPreferences::preferenceChanged_geniusUpdateAlbum(bool value)     {
    geniusTabUpdateAlbumCheckbox->setChecked(value);
}
void TWindowPreferences::preferenceChanged_geniusUpdateTitle(bool value)     {
    geniusTabUpdateTitleCheckbox->setChecked(value);
}
void TWindowPreferences::preferenceChanged_geniusUpdateYear(bool value)      {
    geniusTabUpdateYearCheckbox->setChecked(value);
}
void TWindowPreferences::preferenceChanged_geniusUpdateGenre(bool value)     {
    geniusTabUpdateGenreCheckbox->setChecked(value);
}
void TWindowPreferences::preferenceChanged_geniusUpdateFilename(bool value)  {
    geniusTabUpdateFilenameCheckbox->setChecked(value);
}
void TWindowPreferences::preferenceChanged_geniusSearchWithArtist(bool value)    {
    geniusTabSearchWithArtistCheckbox->setChecked(value);
}
void TWindowPreferences::preferenceChanged_geniusSearchWithAlbum(bool value)     {
    geniusTabSearchWithAlbumCheckbox->setChecked(value);
}
void TWindowPreferences::preferenceChanged_geniusSearchWithTitle(bool value)     {
    geniusTabSearchWithTitleCheckbox->setChecked(value);
}
void TWindowPreferences::preferenceChanged_geniusSearchWithYear(bool value)      {
    geniusTabSearchWithYearCheckbox->setChecked(value);
}
void TWindowPreferences::preferenceChanged_geniusSearchWithGenre(bool value)     {
    geniusTabSearchWithGenreCheckbox->setChecked(value);
}
void TWindowPreferences::preferenceChanged_geniusSearchWithFilename(bool value)  {
    geniusTabSearchWithFilenameCheckbox->setChecked(value);
}
void TWindowPreferences::preferenceChanged_geniusAlwaysUseFirstHit(bool value)  {
    geniusTabAlwaysUseFirstHitCheckbox->setChecked(value);
}
