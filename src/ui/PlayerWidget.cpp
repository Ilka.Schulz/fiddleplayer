/// @file

#include "PlayerWidget.hpp"
#include "src/File.hpp"

PlayerWidget::PlayerWidget(QWidget *parent)
        : QWidget(parent),
          player(TFile::getAllFilesByReference()),
          currentlyMovingHorizontalSlider(false)  {
    // player
    verticalLayout = new QBoxLayout(QBoxLayout::TopToBottom, this);
    verticalLayout->setContentsMargins(0,0,0,0);
    setLayout(verticalLayout);
        // slider layout
        sliderLayout = new QBoxLayout(QBoxLayout::LeftToRight, this);
        sliderLayout->setContentsMargins(0,0,0,0);
        verticalLayout->addLayout(sliderLayout);
            // time stamp label
            timeStampLabel = new QLabel(this);
            timeStampLabel->setText("--:--");
            sliderLayout->addWidget(timeStampLabel);
            // horizontal slider
            horizontalSlider = new QSlider(this);
            horizontalSlider->setEnabled(false);
            horizontalSlider->setOrientation(Qt::Orientation::Horizontal);
            horizontalSlider->setTickInterval(5000);
            horizontalSlider->setSingleStep(5000);
            horizontalSlider->setPageStep(5000);
            horizontalSlider->setTracking(false);  // only emit valueChanged signal *after* the user drops the slider
            sliderLayout->addWidget(horizontalSlider);
            connect(horizontalSlider, &QSlider::valueChanged, this, &PlayerWidget::horizontalSliderValueChangedEvent);
            // duration label
            durationLabel = new QLabel(this);
            durationLabel->setText("--:--");
            sliderLayout->addWidget(durationLabel);
        // buttons layout
        buttonsLayout = new QBoxLayout(QBoxLayout::LeftToRight, this);
        buttonsLayout->setAlignment(Qt::AlignLeading);
        buttonsLayout->setContentsMargins(0,0,0,0);
        verticalLayout->addLayout(buttonsLayout);
            // play / pause button
            playButton = new QToolButton(this);
            playButton->setText("Play");
            buttonsLayout->addWidget(playButton);
            connect(playButton, &QToolButton::clicked, this, &PlayerWidget::playButtonClickedEvent);
            // previous button
            previousButton = new QToolButton(this);
            previousButton->setText("Previous");
            previousButton->setEnabled(false);
            buttonsLayout->addWidget(previousButton);
            connect(previousButton, &QToolButton::clicked, this, &PlayerWidget::previousButtonClickedEvent);
            // stop button
            stopButton = new QToolButton(this);
            stopButton->setText("Stop");
            buttonsLayout->addWidget(stopButton);
            connect(stopButton, &QToolButton::clicked, this, &PlayerWidget::stopButtonClickedEvent);
            // next button
            nextButton = new QToolButton(this);
            nextButton->setText("Next");
            nextButton->setEnabled(false);
            buttonsLayout->addWidget(nextButton);
            connect(nextButton, &QToolButton::clicked, this, &PlayerWidget::nextButtonClickedEvent);
            // loop button
            loopButton = new QToolButton(this);
            loopButton->setText("Loop");
            loopButton->setEnabled(false);
            buttonsLayout->addWidget(loopButton);
            connect(loopButton, &QToolButton::clicked, this, &PlayerWidget::loopButtonClickedEvent);
            // shuffle button
            shuffleButton = new QToolButton(this);
            shuffleButton->setText("Shuffle");
            shuffleButton->setEnabled(false);
            buttonsLayout->addWidget(shuffleButton);
            connect(shuffleButton, &QToolButton::clicked, this, &PlayerWidget::shuffleButtonClickedEvent);

    player.addObserver(this);
}
PlayerWidget::~PlayerWidget()  {
    player.removeObserver(this);
}

// public methods
void PlayerWidget::selectFile(TFile *file)  {
    player.selectFile(file);
}
void PlayerWidget::start()  {
    player.start();
}


// private methods
QString PlayerWidget::timeToString(qint64 timeStamp)  {
    if(timeStamp == -1)
        return "--/--";
    int seconds = timeStamp/1000;
    int hours   = seconds/3600;
    int minutes = (seconds/60)%60;
    seconds = seconds%60;
    QString hoursString   = hours!=0 ? QString::number(hours)+":" : "";
    QString minutesString = QString::number(minutes) + ":";
    if(minutesString.size()<3)
        minutesString = "0"+minutesString;
    QString secondsString = QString::number(seconds);
    if(secondsString.size()<2)
        secondsString = "0"+secondsString;
    return hoursString+minutesString+secondsString;
}

// slots
void PlayerWidget::playButtonClickedEvent(bool)  {
    if(player.isPlaying())
        player.pause();
    else
        player.start();
}
void PlayerWidget::previousButtonClickedEvent(bool)  {
    /// @todo implement
}
void PlayerWidget::stopButtonClickedEvent(bool)  {
    player.stop();
}
void PlayerWidget::nextButtonClickedEvent(bool)  {
    /// @todo implement
}
void PlayerWidget::loopButtonClickedEvent(bool)  {
    /// @todo implement
}
void PlayerWidget::shuffleButtonClickedEvent(bool)  {
    player.setShuffleMode( ! player.getShuffleMode() );
}
void PlayerWidget::horizontalSliderValueChangedEvent(int value)  {
    if(currentlyMovingHorizontalSlider)
        return;
    player.moveToTimeStamp(value);
}

// observer events
void PlayerWidget::playerEvent_fileSelected(Player *)  {
    /// @todo deselect old TFile object
    /// @todo choose better highlighting method, e.g. by setting bold text
    player.getCurrentlyPlayingFile()->setSelected(true);
}
void PlayerWidget::playerEvent_started(Player *)  {
    playButton->setText(tr("Pause"));
    horizontalSlider->setEnabled(true);
}
void PlayerWidget::playerEvent_paused(Player *)  {
    playButton->setText(tr("Play"));
}
void PlayerWidget::playerEvent_timeStampChanged(Player *)  {
    timeStampLabel->setText( timeToString(player.getTimeStamp()) );
    if( ! horizontalSlider->isSliderDown() )  {
        currentlyMovingHorizontalSlider = true;
        horizontalSlider->setValue(player.getTimeStamp());
        currentlyMovingHorizontalSlider = false;
    }
}
void PlayerWidget::playerEvent_durationChanged(Player *)  {
    durationLabel->setText( timeToString(player.getDuration()) );
    horizontalSlider->setMaximum(player.getDuration());
}
void PlayerWidget::playerEvent_stopped(Player *)  {
    timeStampLabel->setText( timeToString(-1) );
    durationLabel->setText( timeToString(-1) );
    horizontalSlider->setEnabled(false);
    playButton->setText(tr("Play"));
}
void PlayerWidget::playerEvent_loopModeChanged(Player *)  {
    /// @todo implement
}
void PlayerWidget::playerEvent_shuffleModeChanged(Player *)  {
    /// @todo implement
}
void PlayerWidget::playerEvent_volumeChanged(Player *)  {
    /// @todo implement
}
