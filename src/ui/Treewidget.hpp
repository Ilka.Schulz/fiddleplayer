#pragma once
#include <QTreeWidget>


class TTreeWidget : public QTreeWidget
{
    Q_OBJECT
public:
    explicit TTreeWidget(QWidget *parent = nullptr);

private:
    void keyPressEvent(QKeyEvent *event) override;

signals:

public slots:
};
