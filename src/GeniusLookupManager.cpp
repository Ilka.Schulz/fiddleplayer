/// @file

#include "GeniusLookupManager.hpp"

#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>

#include "src/util/Exception.hpp"
#include "src/File.hpp"
#include "src/ui/Windowdecideurl.hpp"

#include "src/Preferences.hpp"


// constructor & destructor
GeniusLookupManager::GeniusLookupManager(QObject *parent)
        : QNetworkAccessManager(parent),
          timer(this)  {
    // timer
    connect(&timer, &QTimer::timeout, this, &GeniusLookupManager::on_timeout);
    timer.setInterval(10);
    timer.start();
    // network access manager
    connect(this, &QNetworkAccessManager::finished, this, &GeniusLookupManager::on_networkFinished);
}

// public methods
void GeniusLookupManager::lookUp(TFile *file)  {
    //QString title = f->getTitle(),
    //        artist = f->getArtist();
    //if(title.size()==0 || artist.size()==0)
    //    ERROR("cannot search for data when artist and title are unknown ("+f->getFn()+")");
    QString searchstring = "";
    if(Preferences::getInstance()->getSetting_geniusSearchWithArtist())
        searchstring += file->getArtist()+" ";
    if(Preferences::getInstance()->getSetting_geniusSearchWithAlbum())
        searchstring += file->getAlbum()+" ";
    if(Preferences::getInstance()->getSetting_geniusSearchWithTitle())
        searchstring += file->getTitle()+" ";
    if(Preferences::getInstance()->getSetting_geniusSearchWithYear())
        searchstring += file->getYear()+" ";
    if(Preferences::getInstance()->getSetting_geniusSearchWithGenre())
        searchstring += file->getGenre()+" ";
    /// @todo search with filename

    searchstring.replace("&","");
    searchstring.replace("\"","");
    QUrl url("https://html.duckduckgo.com/html?q=domain:genius.com "+searchstring);  /// @todo duckduckgo behaves strangely when specifying genius.com as domain
    network_requests[url.toEncoded()] = file;
    this->get(QNetworkRequest(url));
    //QThread::sleep(1);
}
int GeniusLookupManager::getActiveRequestsCount() const  {
    return network_requests.size() - network_request_todo.size();
}
int GeniusLookupManager::getQueuedRequestsCount() const  {
    return network_request_todo.size();
}

// private auxiliary methods
map<QString,QString> GeniusLookupManager::readTags(QString raw)  {
    // parse JSON
    std::map<QString,QString> retval;
    QJsonParseError *jsonError = new QJsonParseError;
    QJsonDocument json = QJsonDocument::fromJson(raw.toUtf8(), jsonError);
    if(jsonError->error != QJsonParseError::NoError)
        ERROR("invalid json");
    if( ! json.isObject() )
        ERROR("json is no object");
    QJsonObject jsonObject = json.object();
    QJsonValue song = jsonObject.value("song");
    if(song.type() != QJsonValue::Object)
        ERROR("song is not an object");
    QJsonValue trackingData = song.toObject().value("tracking_data");
    if(trackingData.type() != QJsonValue::Array)
        ERROR("tracking_data is not an array");

    QJsonArray array = trackingData.toArray();
    for(int i=0; i<array.size(); i++)  {
        const auto & val = array.at(i);
        if(val.isObject())  {
            QJsonObject pair = val.toObject();
            if(pair.size() == 2)  {
                QJsonValue key = pair.value("key");
                QJsonValue val = pair.value("value");
                if( ! val.isUndefined()  && ! key.isUndefined() )  {
                    QString valString;
                    switch(val.type())  {
                    case QJsonValue::Bool:
                        valString = val.toBool() ? "true" : "false";
                        break;
                    case QJsonValue::Double:
                        valString = QString::number(val.toDouble());
                        break;
                    case QJsonValue::Null:
                        valString = "null";
                        break;
                    case QJsonValue::String:
                        valString = val.toString();
                        break;
                    case QJsonValue::Array:
                        continue;
                    case QJsonValue::Object:
                        continue;
                    case QJsonValue::Undefined:
                        continue;
                    }
                    retval[key.toString()] = valString;
                }
            }
        }
    }

    // replace weird characters
    for(auto &pair : retval)  {
        /// @todo replace french characters, etc.
        /// @todo replace slashes and backslashes
        /// @todo parse &#AA; numerical values
        for(int c=0; c<256; c++)
            pair.second = pair.second.replace("&#"+  QString::number(c,10)  +";",QString(static_cast<char>(c)));
        pair.second = pair.second.replace("&amp;","&");
        //pair.second = pair.second.replace("&#39;","'");
        pair.second = pair.second.replace("ä","ae");
        pair.second = pair.second.replace("ö","oe");
        pair.second = pair.second.replace("ü","ue");
        pair.second = pair.second.replace("Ä","Ae");
        pair.second = pair.second.replace("Ö","Oe");
        pair.second = pair.second.replace("Ü","Ue");
        pair.second = pair.second.replace("ß","ss");
        pair.second = pair.second.replace("á","a");
        pair.second = pair.second.replace("à","a");
        pair.second = pair.second.replace("â","a");
        pair.second = pair.second.replace("é","e");
        pair.second = pair.second.replace("è","e");
        pair.second = pair.second.replace("ê","e");
        pair.second = pair.second.replace("í","i");
        pair.second = pair.second.replace("ì","i");
        pair.second = pair.second.replace("î","i");
        pair.second = pair.second.replace("ó","o");
        pair.second = pair.second.replace("ò","o");
        pair.second = pair.second.replace("ô","o");
        pair.second = pair.second.replace("ú","u");
        pair.second = pair.second.replace("ù","u");
        pair.second = pair.second.replace("û","u");
        pair.second = pair.second.replace("°","o");
        for(int c=128; c<256; c++)
            pair.second.remove(static_cast<char>(c));
    }

    return retval;
}
QString GeniusLookupManager::findTag(map<QString, QString> data, QString key)  {
    map<QString,QString>::iterator itr = data.find(key);
    if(itr == data.end())
        return "";
    if(itr->second == "null")
        return "";
    return itr->second;
}
void GeniusLookupManager::on_networkFinished(QNetworkReply *reply)  {
    /// @todo check for errors (if(reply->error()))
    std::map<QString,TFile*>::iterator itr = network_requests.find(reply->url().toEncoded());
    if(itr == network_requests.end())
        ERROR("network request cannot be associated with file");
    TFile *file = itr->second;  /// @todo check whether file has been deleted in the mean time
    network_requests.erase(itr);
    if(reply->url().toString().contains("duckduckgo.com/"))  {  /// @todo magic strings
        QString results_raw = reply->readAll();
        if(results_raw.contains("If this error persists, please let us know: error-lite@duckduckgo.com"))
            ERROR("duckduckgo.com triggered DOS protection...\nFile:"+file->getAbsoluteFn());
        QStringList results_strings = results_raw.split("result__url",QString::SkipEmptyParts);
        QStringList results;
        for(QString result_raw : results_strings)  {
            int idx = result_raw.indexOf('>');
            if(idx==-1)
                ERROR("could not parse search result");
            result_raw = result_raw.right(result_raw.size()-idx-1);
            idx = result_raw.indexOf("<");
            if(idx==-1)
                ERROR("coud not parse search result");
            result_raw = result_raw.left(idx);
            result_raw.replace(" ","");
            result_raw.replace("\t","");
            result_raw.replace("\n","");
            if(result_raw.size()==0)
                continue ;
            if(result_raw.contains("genius.com/albums/") || result_raw.contains("genius.com/artists/"))
                continue ;
            if(!result_raw.contains("genius.com/"))  // advertisements
                continue ;
            /// @todo manage DuckDuckGo DOS protection
            if(! result_raw.startsWith("http"))
                result_raw = "https://"+result_raw;
            results.append(result_raw);
            //cout << ("\""+result_raw+"\"").toUtf8().data()<<endl;
        }
        if(results.size()==0)  {
            ERROR_MSG("could not find song on genius.com ("+file->getTitle()+" by "+file->getArtist()+")");
            return ;
        }
        QString url_str;
        if(results.size()==1 || Preferences::getInstance()->getSetting_geniusAlwaysUseFirstHit())
            url_str = results[0];
        else  {
            url_str = TWindowDecideUrl::execute(file->getArtist(),file->getTitle(),results);
            if(url_str.size()==0)
                return ;  // aborted
        }
        QUrl url(url_str);
        network_requests[url.toEncoded()] = file;
        network_request_todo.push_back(url);
    }
    else if(reply->url().toEncoded().contains("genius.com/"))  {  /// @todo check for genius
        QString raw = reply->readAll();
        int idx = raw.indexOf("{&quot;chartbeat");
        if(idx==-1)
            ERROR("could not parse genius data: no chartbeat available");
        raw = raw.right(raw.size()-idx);
        idx = raw.indexOf("\"");
        if(idx==-1)
            ERROR("could not parse genius data");
        raw = raw.left(idx);
        raw.replace("&quot;","\"");
        //INFO_MESSAGE(raw);
        //cout << raw.toUtf8().data();
        // Primary Artist
        // Primary Album
        // Title
        // created_year
        // Tag
        /*QString artist = readTag(raw,"authors"),
                title  = readTag(raw,"title"),  // careful! This is not the actual song title but the web page title...
                year   = readTag(raw,"release_year"),
                album  = readTag(raw,"albums"),
                genre  = readTag(raw,"genres");*/
        map<QString, QString> tags = readTags(raw);
        QString artist = findTag(tags,"Primary Artist"),
                album  = findTag(tags,"Primary Album"),
                title  = findTag(tags,"Title"),
                year   = findTag(tags,"Release Date"),
                genre  = findTag(tags,"Tag");
        year = year.left(4);
        QMessageBox box;
        box.setWindowTitle(title);
        box.setIcon(QMessageBox::Question);
        box.setText("Are these information correct?\n"
                    "\n"
                    "artist:  \t"+file->getArtist()+"  ==  "+artist+"\n"
                    "title:   \t"+file->getTitle()+"  ==  "+title+"\n"
                    "\n"
                    "album:   \t"+album+"\n"
                    "year:    \t"+year+"\n"
                    "genre:   \t"+genre+"\n");
        box.setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
        if((title.toLower()==file->getTitle().toLower() && artist.toLower()==file->getArtist().toLower()) || box.exec() == QMessageBox::Yes)  {
            if(Preferences::getInstance()->getSetting_geniusUpdateArtist())
                file->setArtist(artist);
            if(Preferences::getInstance()->getSetting_geniusUpdateAlbum())
                file->setAlbum(album);
            if(Preferences::getInstance()->getSetting_geniusUpdateTitle())
                file->setTitle(title);
            if(Preferences::getInstance()->getSetting_geniusUpdateYear())
                file->setYear(year);
            if(Preferences::getInstance()->getSetting_geniusUpdateGenre())
                file->setGenre(genre);  /// @todo start with capital letter
                                        /// @todo remove "Genius"
        }
    }
    else
        ERROR("unknown response domain:\n"+reply->url().toEncoded());
}
void GeniusLookupManager::on_timeout()  {
    if(TWindowDecideUrl::getCurrentInstances() == 0
    && ! network_request_todo.empty() )  {
        this->get(QNetworkRequest(network_request_todo[0]));
        network_request_todo.erase(network_request_todo.begin());
    }
}
